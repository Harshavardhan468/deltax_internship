DROP DATABASE IMDB;

CREATE DATABASE IMDB;
USE IMDB;


CREATE TABLE Producers (
Id INT PRIMARY KEY IDENTITY(1,1), 
Name VARCHAR(42), 
Company VARCHAR(42), 
CompanyEstDate Date);

INSERT INTO Producers VALUES ('Arjun','Fox','05/14/1998');
INSERT INTO Producers VALUES ('Arun','Bull','09/11/2004');
INSERT INTO Producers VALUES ('Tom','Hanks','11/03/1987');
INSERT INTO Producers VALUES ('Zeshan','Male','11/14/1996');
INSERT INTO Producers VALUES ('Nicole','Team Coco','09/26/1992');

INSERT INTO Producers VALUES ('Jeet','DeltaX','09/26/1992');

CREATE TABLE Actors (
Id INT PRIMARY KEY IDENTITY(1,1), 
Name VARCHAR(42), 
Gender VARCHAR(10), 
DOB Date);

INSERT INTO Actors VALUES ('Mila Kunis','Female','11/14/1986');
INSERT INTO Actors VALUES ('Robert DeNiro','Male','07/10/1957');
INSERT INTO Actors VALUES ('George Michael','Male','11/23/1978');
INSERT INTO Actors VALUES ('Mike Scott','Male','08/06/1969');
INSERT INTO Actors VALUES ('Pam Halpert','Female','09/26/1996');
INSERT INTO Actors VALUES ('Dame Judi Dench','Female','04/05/1947');

--INSERT INTO Actors VALUES ('Robert1 DeNiro','Male','07/10/1957');
--DELETE FROM Actors WHERE Id = 7;

CREATE TABLE Movies (
Id INT PRIMARY KEY IDENTITY(1,1), 
Name VARCHAR(42), 
Language VARCHAR(15), 
ProducerId INT FOREIGN KEY REFERENCES Producers(Id), 
Profit MONEY);

INSERT INTO Movies VALUES ('Rocky','English',1,10000);
INSERT INTO Movies VALUES ('Rocky','Hindi',3,3000);
INSERT INTO Movies VALUES ('Terminal','English',4,300000);
INSERT INTO Movies VALUES ('Rambo','Hindi',2,93000);
INSERT INTO Movies VALUES ('Rudy','English',5,9600);

--INSERT INTO Movies VALUES (6,'Rudy','Hindi',1,9600);
--DELETE FROM Movies WHERE Id = 6;

CREATE TABLE MovieActorMapping(
Id INT PRIMARY KEY IDENTITY(1,1),
MovieId INT FOREIGN KEY REFERENCES Movies(Id), 
ActorId INT FOREIGN KEY REFERENCES Actors(Id));

INSERT INTO MovieActorMapping VALUES(1,1);
INSERT INTO MovieActorMapping VALUES(1,3);
INSERT INTO MovieActorMapping VALUES(1,5);
INSERT INTO MovieActorMapping VALUES(2,6);
INSERT INTO MovieActorMapping VALUES(2,5);
INSERT INTO MovieActorMapping VALUES(2,4);
INSERT INTO MovieActorMapping VALUES(2,2);
INSERT INTO MovieActorMapping VALUES(3,3);
INSERT INTO MovieActorMapping VALUES(3,2);
INSERT INTO MovieActorMapping VALUES(4,1);
INSERT INTO MovieActorMapping VALUES(4,6);
INSERT INTO MovieActorMapping VALUES(4,3);
INSERT INTO MovieActorMapping VALUES(5,2);
INSERT INTO MovieActorMapping VALUES(5,5);
INSERT INTO MovieActorMapping VALUES(5,3);

--INSERT INTO MovieActorMapping VALUES(6,1); 
--INSERT INTO MovieActorMapping VALUES(6,3); 
--INSERT INTO MovieActorMapping VALUES(6,5); 
--DELETE FROM MovieActorMapping WHERE Id = 16;
--DELETE FROM MovieActorMapping WHERE Id = 17;
--DELETE FROM MovieActorMapping WHERE Id = 18;

SELECT * FROM Movies;
SELECT * FROM Actors;
SELECT * FROM Producers;
SELECT * FROM MovieActorMapping;

-- 1) Update Profit of all the movies by +1000 where producer name contains 'run'

-- APPROACH 1
UPDATE Movies 
SET Profit = Profit + 1000
WHERE Id IN
	(SELECT M.Id
	 FROM Producers P
	 INNER JOIN Movies M
	 ON M.ProducerId = P.Id
	 WHERE P.Name LIKE '%run%');

-- APPROACH 2
UPDATE Movies 
SET Profit = Profit + 1000
FROM Producers P
INNER JOIN Movies M
ON M.ProducerId = P.Id
WHERE P.Name LIKE '%run%';

-- 2) Find duplicate movies having the same name and their count

SELECT M.Name, COUNT(M.Name) 
FROM Movies M
GROUP BY M.Name
HAVING COUNT(M.Name) > 1;

-- 3) Find the oldest actor/actress for each movie

SELECT M.Id,M.Name, M.Language, (SELECT TOP 1 Name FROM Actors WHERE DOB = MIN(A.DOB)) AS [ACTOR NAME], MIN(A.DOB) AS[ACTOR DOB]
from MovieActorMapping MAM
JOIN Actors A
ON MAM.ActorId = A.Id
JOIN Movies M
ON MAM.MovieId = M.Id
GROUP BY M.Id, M.Name, M.Language

-- 4) List of producers who have not worked with actor X

-- APPROACH 1
Select P.Name 
FROM Producers P
WHERE P.Id NOT IN (SELECT M.ProducerId
					FROM Movies M
					JOIN MovieActorMapping MAM
					ON MAM.MovieId=M.Id
					Join Actors A
					ON A.Id=MAM.ActorId
					WHERE A.Name = 'Mila Kunis')

-- APPROACH 2
SELECT M.ProducerId
FROM Movies M
EXCEPT
SELECT M.ProducerId
FROM Movies M
JOIN MovieActorMapping MAM
ON MAM.MovieId=M.Id
Join Actors A
ON A.Id=MAM.ActorId
WHERE A.Name = 'Mila Kunis'

-- 5) List of pair of actors who have worked together in more than 2 movies

-- APPROACH 1
GO
CREATE FUNCTION GetMoviesCount( @AId1 INT, @AId2 INT)
RETURNS INT 
AS
	BEGIN
		RETURN (SELECT COUNT(*) FROM Movies M
		WHERE M.Id IN (SELECT MAM.MovieId
							FROM MovieActorMapping MAM
							Join Actors A
							ON A.Id=MAM.ActorId
							WHERE a.Id = 1)
			AND M.Id IN (SELECT MAM.MovieId
							FROM MovieActorMapping MAM
							Join Actors A
							ON A.Id=MAM.ActorId
							WHERE A.Id = 3))
	END
GO

DROP FUNCTION GetMoviesCount

SELECT A1.Name, A2.Name, dbo.GetMoviesCount(A1.Id , A2.Id) AS [COUNT OF MOVIES]
FROM Actors A1
JOIN Actors A2
On A1.Id > A2.Id
WHERE dbo.GetMoviesCount(A1.Id , A2.Id) >= 2;

-- APPROACH 2

SELECT A1.Name,A2.Name
FROM Actors A1, Actors A2 ,MovieActorMapping M1, MovieActorMapping M2 
WHERE A1.id=M1.ActorId AND A2.Id=M2.ActorId and  M1.MovieId=M2.MovieId and A1.Id<A2.Id
GROUP BY A1.NAME,A2.NAME
HAVING COUNT(M1.MovieId) > 1

--APPROACH 3

SELECT A1.Name, A2.Name, COUNT(MAM1.MovieId)
FROM Actors A1
JOIN MovieActorMapping MAM1
ON A1.Id = MAM1.ActorId 
JOIN MovieActorMapping MAM2
ON MAM1.MovieId = MAM2.MovieId 
JOIN Actors A2
ON A2.Id = MAM2.ActorId 
WHERE A1.Id < A2.Id
GROUP BY A1.NAME,A2.NAME
HAVING COUNT(MAM1.MovieId) > 1

-- 6) Add non-clustered index on profit column of movies table

SELECT * FROM Movies;

CREATE NONCLUSTERED INDEX IX_Movies_Profit ON Movies(Profit ASC);

-- 7) Create stored procedure to return list of actors for given movie id

GO
CREATE PROCEDURE GetActors @MovieId INT
AS
	SELECT A.* 
	FROM MovieActorMapping MAM
	JOIN Actors A
	ON MAM.ActorId = A.Id
	WHERE MAM.MovieId = @MovieId;
GO

EXECUTE  GetActors 6;  

-- 8) Create a function to return age for given date of birth

CREATE FUNCTION GetAge(@DOB Date)  
RETURNS INT 
AS   
BEGIN  
    RETURN DATEDIFF(DAY,@DOB,GETDATE())/365
END

DROP FUNCTION GetAge  

SELECT dbo.GetAge('1970/06/03') AS [AGE]

-- 9) Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 

GO
CREATE PROCEDURE IncreaseProfit @Amount MONEY, @Ids VARCHAR(40)
AS
UPDATE Movies 
Set Profit = Profit + @Amount 
WHERE Id IN 
(SELECT * FROM STRING_SPLIT( @Ids , ',' ));
GO

EXECUTE  IncreaseProfit 10000,'1,2,3';  
SELECT * FROM Movies;

-- 10) Give the count for movies in which a Producer and Actor pair worked with

SELECT P.Name,A.Name,COUNT(M.Id)
FROM Producers P
JOIN Movies M
ON P.Id=M.ProducerId
JOIN MovieActorMapping AM
ON M.Id=AM.MovieId
JOIN Actors A
ON A.Id=AM.ActorId
GROUP BY P.Name,A.Name

-- 11) Give the Oldest Actor for a movie in male and female 

SELECT M.Id,M.Name, M.Language, (SELECT TOP 1 Name FROM Actors WHERE DOB = MIN(A.DOB)) AS [ACTOR NAME], MIN(A.DOB) AS[ACTOR DOB] ,A.Gender
from MovieActorMapping MAM
JOIN Actors A
ON MAM.ActorId = A.Id
JOIN Movies M
ON MAM.MovieId = M.Id
GROUP BY M.Id, M.Name, M.Language, A.Gender
ORDER BY M.Id

-- 12) Give the Oldest Actor for a movie in male

SELECT M.Id,M.Name, M.Language, (SELECT TOP 1 Name FROM Actors WHERE DOB = MIN(A.DOB)) AS [ACTOR NAME], MIN(A.DOB) AS[ACTOR DOB] ,A.Gender
from MovieActorMapping MAM
JOIN Actors A
ON MAM.ActorId = A.Id
JOIN Movies M
ON MAM.MovieId = M.Id
WHERE A.Gender = 'Male'
GROUP BY M.Id, M.Name, M.Language, A.Gender

-- 13) Give the Oldest Actor for a movie in female

SELECT M.Id,M.Name, M.Language, (SELECT TOP 1 Name FROM Actors WHERE DOB = MIN(A.DOB)) AS [ACTOR NAME], MIN(A.DOB) AS[ACTOR DOB] ,A.Gender
from MovieActorMapping MAM
JOIN Actors A
ON MAM.ActorId = A.Id
JOIN Movies M
ON MAM.MovieId = M.Id
WHERE A.Gender = 'Female'
GROUP BY M.Id, M.Name, M.Language, A.Gender

-- 14) List of actors and corresponding total profit

SELECT A.Id, A.Name, CONCAT(STR(FLOOR(SUM(Profit)/1000)),'XXX') AS [TOTAL PROFIT]
FROM Actors A
JOIN MovieActorMapping MAM
ON A.Id = MAM.ActorId
JOIN Movies M
ON M.Id = MAM.MovieId
GROUP BY A.Id, A.Name

-- 15) list down the all the actors and Count Of Movies

SELECT A.Name, COUNT(MAM.MovieId)
FROM Actors A
LEFT JOIN MovieActorMapping MAM
ON A.Id = MAM.ActorId
GROUP BY A.Id, A.Name


SELECT P.Name
FROM Producers P
LEFT JOIN Movies M
ON P.Id = M.ProducerId
WHERE M.Id IS NULL