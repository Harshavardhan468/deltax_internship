DROP DATABASE School

CREATE DATABASE School
USE School

CREATE TABLE Classes (
Id INT PRIMARY KEY IDENTITY(1,1), 
Name VARCHAR(42), 
Section VARCHAR(1), 
Number INT)

CREATE TABLE Teachers (
Id INT Primary KEY IDENTITY(1,1), 
Name VARCHAR(42), 
DOB DATE, 
Gender VARCHAR(6))

CREATE TABLE Students (
Id INT Primary KEY IDENTITY(1,1), 
Name VARCHAR(42), 
DOB DATE, 
Gender VARCHAR(6), 
ClassId INT FOREIGN KEY REFERENCES Classes(Id))

CREATE TABLE TeacherClassMapping (
Id INT PRIMARY KEY IDENTITY(1,1),
TeacherId INT FOREIGN KEY REFERENCES Teachers(Id), 
ClassId INT FOREIGN KEY REFERENCES Classes(Id))

INSERT INTO Classes values ('IX','A',201)
INSERT INTO Classes values ('IX','B',202)
INSERT INTO Classes values ('X','A',203)

INSERT INTO Teachers values ('Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teachers values ('Monica Bing','1982/03/06','Female')
INSERT INTO Teachers values ('Chandler Bing','1978/12/17','Male')
INSERT INTO Teachers values ('Ross Geller','1993/01/26','Male')

INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)

INSERT INTO TeacherClassMapping values (1,1)
INSERT INTO TeacherClassMapping values (1,2)
INSERT INTO TeacherClassMapping values (2,2)
INSERT INTO TeacherClassMapping values (2,3)
INSERT INTO TeacherClassMapping values (3,3)
INSERT INTO TeacherClassMapping values (3,1)


SELECT * FROM Classes;
SELECT * FROM Teachers;
SELECT * FROM Students;

-- 1)Find list of male students

SELECT * 
FROM Students 
where Gender = 'Male';

-- 2)Find list of student older than 2005/01/01

SELECT * 
FROM Students where DOB < '2005/01/01';

-- 3)Youngest student in school

--APPROACH 1
SELECT * 
FROM Students 
where DOB = (SELECT MAX(DOB) FROM Students);

--APPROACH 2
SELECT TOP 1 *
FROM Students 
ORDER BY DOB DESC;

-- 4)Find student distinct birthdays

SELECT DISTINCT(DOB) 
FROM Students;

-- 5)No of students in each class

SELECT  C.Name AS Class, Count(S.Id) AS CountOfStudents
FROM Classes C
INNER JOIN Students S
ON C.Id = S.ClassId
GROUP BY C.Name;

-- 6)No of students in each section for each class

SELECT C.Name AS Class, C.Section AS Section, Count(S.Id) AS CountOfStudents
FROM Classes C
INNER JOIN Students S
ON C.Id = S.ClassId
GROUP BY C.Name,C.Section;

-- 7)No of students in each section

SELECT C.Section AS Section, Count(S.Id) AS CountOfStudents
FROM Classes C
INNER JOIN Students S
ON C.Id = S.ClassId
GROUP BY C.Section;

-- 8)No of classes taught by teacher

SELECT T.Name, Count(*) AS CountOfClasses
FROM TeacherClassMapping TCM
JOIN Classes C 
ON TCM.ClassId = C.Id
JOIN Teachers T 
ON TCM.TeacherId = T.Id
GROUP BY T.Id,T.Name;

-- 9)List of teachers teaching Class X

SELECT DISTINCT(T.Name) AS [TEACHER NAMES]
FROM TeacherClassMapping TCM
JOIN Classes C 
ON TCM.ClassId = C.Id
JOIN Teachers T 
ON TCM.TeacherId = T.Id
WHERE C.Name = 'X';

-- 10)Classes which have more than 2 teachers teaching

SELECT C.Name AS ClassName
FROM TeacherClassMapping TCM
JOIN Classes C 
ON TCM.ClassId = C.Id
JOIN Teachers T 
ON TCM.TeacherId = T.Id
GROUP BY C.Name
HAVING COUNT(T.Id)>2;

-- 11)List of students being taught by 'Lisa'

SELECT S.Name AS StudentName
FROM Students S
Join TeacherClassMapping TCM 
ON TCM.ClassId=S.ClassId
Join Teachers T 
ON TCM.TeacherId=T.Id
WHERE T.Name LIKE 'Lisa%';
