﻿Feature: ImdbActors

@condition1
Scenario: Adding a Actor.
    Given Actor name "Tom cruze"
    And The Actor Date "03/07/2000"
    When We Add Actor To Repository
    Then Actors Table Look Like
    | Id | Name               | DOB        |
    | 1  | Robert Downey Jr   | 04/04/1965 |
    | 2  | Chris Evans        | 13/06/1981 |
    | 3  | Scarlett Johansson | 22/11/1984 |
    | 4  | Tom cruze          | 03/07/2000 |

@condition1
Scenario: Adding a null or empty Actor name.
    Given Actor name ""
    And The Actor Date "03/07/2000"
    When We Add Actor To Repository
    Then I should have an error for actors"Invalid arguments"
    And Actors Table Look Like
    | Id | Name               | DOB        |
    | 1  | Robert Downey Jr   | 04/04/1965 |
    | 2  | Chris Evans        | 13/06/1981 |
    | 3  | Scarlett Johansson | 22/11/1984 |

@condition1
Scenario: Adding a null or empty Actor Dob.
    Given Actor name "Tom cruze"
    And The Actor Date ""
    When We Add Actor To Repository
    Then I should have an error for actors"Invalid actor DOB"
    And Actors Table Look Like
    | Id | Name               | DOB        |
    | 1  | Robert Downey Jr   | 04/04/1965 |
    | 2  | Chris Evans        | 13/06/1981 |
    | 3  | Scarlett Johansson | 22/11/1984 |

@condition1
Scenario: Adding a Invalid Actor DOB.
    Given Actor name "Tom cruze"
    And The Actor Date "03/072000"
    When We Add Actor To Repository
    Then I should have an error for actors"Invalid actor DOB"
    And Actors Table Look Like
    | Id | Name               | DOB        |
    | 1  | Robert Downey Jr   | 04/04/1965 |
    | 2  | Chris Evans        | 13/06/1981 |
    | 3  | Scarlett Johansson | 22/11/1984 |