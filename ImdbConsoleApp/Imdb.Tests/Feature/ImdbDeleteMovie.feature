﻿Feature: ImdbDeleteMovie

@condition1
Scenario: Deleting a Movie.
    Given Movie Id "1"
    When We Delete Movie From Repository
    Then Movie Table Look Like
    | Id | Name                               | Plot                                    | Date       |
    | 2  | Iron Man 2                         | Iron Man Updated the suit               | 30/04/2010 |
    | 3  | Iron Man 3                         | Iron Man Created the suit 2.0           | 25/04/2013 |
    | 4  | Captain America: The First Avenger | Captain America Born                    | 25/04/2013 |
    | 5  | captain america civil war          | Captain America Fought                  | 25/04/2013 |
    | 6  | Captain America: Winter soldier    | Captain America and iron became friends | 30/04/2010 |
    And Actors are After Deleting
    | movieId | actorList                                       |
    | 2       | Chris Evans,Scarlett Johansson                  |
    | 3       | Chris Evans,Scarlett Johansson                  |
    | 4       | Robert Downey Jr,Chris Evans                    |
    | 5       | Robert Downey Jr,Scarlett Johansson             |
    | 6       | Robert Downey Jr                                |
    And Producers are After Deleting
    | movieId | producerId | producerName |
    | 2       | 2          | Robert2      |
    | 3       | 3          | Robert3      |
    | 4       | 2          | Robert2      |
    | 5       | 1          | Robert1      |
    | 6       | 2          | Robert2      |


@condition1
Scenario: Deleting a Movie When Invalid Id is given.
    Given Movie Id "9"
    When We Delete Movie From Repository
    Then I should have an error for Movies "Movie Id Doesn't exists"
    And Movie Table Look Like
    | Id | Name                               | Plot                                    | Date       |
    | 1  | Iron Man 1                         | Iron Man Created the suit               | 02/05/2008 |
    | 2  | Iron Man 2                         | Iron Man Updated the suit               | 30/04/2010 |
    | 3  | Iron Man 3                         | Iron Man Created the suit 2.0           | 25/04/2013 |
    | 4  | Captain America: The First Avenger | Captain America Born                    | 25/04/2013 |
    | 5  | captain america civil war          | Captain America Fought                  | 25/04/2013 |
    | 6  | Captain America: Winter soldier    | Captain America and iron became friends | 30/04/2010 |
    And Actors are After Deleting
    | movieId | actorList                                       |
    | 1       | Robert Downey Jr,Chris Evans                    |
    | 2       | Chris Evans,Scarlett Johansson                  |
    | 3       | Chris Evans,Scarlett Johansson                  |
    | 4       | Robert Downey Jr,Chris Evans                    |
    | 5       | Robert Downey Jr,Scarlett Johansson             |
    | 6       | Robert Downey Jr                                |
    And Producers are After Deleting
    | movieId | producerId | producerName |
    | 1       | 1          | Robert1      |
    | 2       | 2          | Robert2      |
    | 3       | 3          | Robert3      |
    | 4       | 2          | Robert2      |
    | 5       | 1          | Robert1      |
    | 6       | 2          | Robert2      |