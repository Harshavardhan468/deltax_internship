﻿Feature: ImdbAddMovies

@condition1
Scenario: Adding a Movie.
    Given Movie name "Harry Potter"
    And The Movie Plot "Harry born"
    And The Movie Date "03/07/2000"
    And The Movie Actors "1 2 3"
    And The Movie Producer "1"
    When We Add Movie To Repository
    Then Table Look Like
    | Id | Name                               | Plot                                    | Date       |
    | 1  | Iron Man 1                         | Iron Man Created the suit               | 02/05/2008 |
    | 2  | Iron Man 2                         | Iron Man Updated the suit               | 30/04/2010 |
    | 3  | Iron Man 3                         | Iron Man Created the suit 2.0           | 25/04/2013 |
    | 4  | Captain America: The First Avenger | Captain America Born                    | 25/04/2013 |
    | 5  | captain america civil war          | Captain America Fought                  | 25/04/2013 |
    | 6  | Captain America: Winter soldier    | Captain America and iron became friends | 30/04/2010 |
    | 7  | Harry Potter                       | Harry born                              | 03/07/2000 |
    And Actors are
    | movieId | actorIds | actorList                                       |
    | 1       | 1,2      | Robert Downey Jr,Chris Evans                    |
    | 2       | 2,3      | Chris Evans,Scarlett Johansson                  |
    | 3       | 2,3      | Chris Evans,Scarlett Johansson                  |
    | 4       | 1,2      | Robert Downey Jr,Chris Evans                    |
    | 5       | 1,3      | Robert Downey Jr,Scarlett Johansson             |
    | 6       | 1        | Robert Downey Jr                                |
    | 7       | 1,2,3    | Robert Downey Jr,Chris Evans,Scarlett Johansson |
    And Producers are
    | movieId | producerId | producerName |
    | 1       | 1          | Robert1      |
    | 2       | 2          | Robert2      |
    | 3       | 3          | Robert3      |
    | 4       | 2          | Robert2      |
    | 5       | 1          | Robert1      |
    | 6       | 2          | Robert2      |
    | 7       | 1          | Robert1      |

@condition1
Scenario: Adding a null or empty Movie Name.
	Given Movie name ""
    And The Movie Plot "Harry born"
    And The Movie Date "03/07/2000"
    And The Movie Actors "1 2 3"
    And The Movie Producer "1"
    When We Add Movie To Repository
    Then I should have an error "Invalid arguments"
    Then Table Look Like
    | Id | Name                               | Plot                                    | Date       |
    | 1  | Iron Man 1                         | Iron Man Created the suit               | 02/05/2008 |
    | 2  | Iron Man 2                         | Iron Man Updated the suit               | 30/04/2010 |
    | 3  | Iron Man 3                         | Iron Man Created the suit 2.0           | 25/04/2013 |
    | 4  | Captain America: The First Avenger | Captain America Born                    | 25/04/2013 |
    | 5  | captain america civil war          | Captain America Fought                  | 25/04/2013 |
    | 6  | Captain America: Winter soldier    | Captain America and iron became friends | 30/04/2010 |
    And Actors are
    | movieId | actorIds | actorList                           |
    | 1       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 2       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 3       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 4       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 5       | 1,3      | Robert Downey Jr,Scarlett Johansson |
    | 6       | 1        | Robert Downey Jr                    |
    And Producers are
    | movieId | producerId | producerName |
    | 1       | 1          | Robert1      |
    | 2       | 2          | Robert2      |
    | 3       | 3          | Robert3      |
    | 4       | 2          | Robert2      |
    | 5       | 1          | Robert1      |
    | 6       | 2          | Robert2      |

@condition1
Scenario: Adding a null or empty Movie Plot.
	Given Movie name "Harry Potter"
    And The Movie Plot ""
    And The Movie Date "03/07/2000"
    And The Movie Actors "1 2 3"
    And The Movie Producer "1"
    When We Add Movie To Repository
    Then I should have an error "Invalid arguments"
    Then Table Look Like
    | Id | Name                               | Plot                                    | Date       |
    | 1  | Iron Man 1                         | Iron Man Created the suit               | 02/05/2008 |
    | 2  | Iron Man 2                         | Iron Man Updated the suit               | 30/04/2010 |
    | 3  | Iron Man 3                         | Iron Man Created the suit 2.0           | 25/04/2013 |
    | 4  | Captain America: The First Avenger | Captain America Born                    | 25/04/2013 |
    | 5  | captain america civil war          | Captain America Fought                  | 25/04/2013 |
    | 6  | Captain America: Winter soldier    | Captain America and iron became friends | 30/04/2010 |
    And Actors are
    | movieId | actorIds | actorList                           |
    | 1       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 2       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 3       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 4       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 5       | 1,3      | Robert Downey Jr,Scarlett Johansson |
    | 6       | 1        | Robert Downey Jr                    |
    And Producers are
    | movieId | producerId | producerName |
    | 1       | 1          | Robert1      |
    | 2       | 2          | Robert2      |
    | 3       | 3          | Robert3      |
    | 4       | 2          | Robert2      |
    | 5       | 1          | Robert1      |
    | 6       | 2          | Robert2      |

@condition1
Scenario: Adding a invalid Movie Date.
	Given Movie name "Harry Potter"
    And The Movie Plot "Harry born"
    And The Movie Date ""
    And The Movie Actors "1 2 3"
    And The Movie Producer "1"
    When We Add Movie To Repository
    Then I should have an error "Invalid movie date"
    Then Table Look Like
    | Id | Name                               | Plot                                    | Date       |
    | 1  | Iron Man 1                         | Iron Man Created the suit               | 02/05/2008 |
    | 2  | Iron Man 2                         | Iron Man Updated the suit               | 30/04/2010 |
    | 3  | Iron Man 3                         | Iron Man Created the suit 2.0           | 25/04/2013 |
    | 4  | Captain America: The First Avenger | Captain America Born                    | 25/04/2013 |
    | 5  | captain america civil war          | Captain America Fought                  | 25/04/2013 |
    | 6  | Captain America: Winter soldier    | Captain America and iron became friends | 30/04/2010 |
    And Actors are
    | movieId | actorIds | actorList                           |
    | 1       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 2       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 3       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 4       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 5       | 1,3      | Robert Downey Jr,Scarlett Johansson |
    | 6       | 1        | Robert Downey Jr                    |
    And Producers are
    | movieId | producerId | producerName |
    | 1       | 1          | Robert1      |
    | 2       | 2          | Robert2      |
    | 3       | 3          | Robert3      |
    | 4       | 2          | Robert2      |
    | 5       | 1          | Robert1      |
    | 6       | 2          | Robert2      |

@condition1
Scenario: Adding a null or empty Movie Date.
	Given Movie name "Harry Potter"
    And The Movie Plot "Harry born"
    And The Movie Date "03/072000"
    And The Movie Actors "1 2 3"
    And The Movie Producer "1"
    When We Add Movie To Repository
    Then I should have an error "Invalid movie date"
    Then Table Look Like
    | Id | Name                               | Plot                                    | Date       |
    | 1  | Iron Man 1                         | Iron Man Created the suit               | 02/05/2008 |
    | 2  | Iron Man 2                         | Iron Man Updated the suit               | 30/04/2010 |
    | 3  | Iron Man 3                         | Iron Man Created the suit 2.0           | 25/04/2013 |
    | 4  | Captain America: The First Avenger | Captain America Born                    | 25/04/2013 |
    | 5  | captain america civil war          | Captain America Fought                  | 25/04/2013 |
    | 6  | Captain America: Winter soldier    | Captain America and iron became friends | 30/04/2010 |
    And Actors are
    | movieId | actorIds | actorList                           |
    | 1       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 2       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 3       | 2,3      | Chris Evans,Scarlett Johansson      |
    | 4       | 1,2      | Robert Downey Jr,Chris Evans        |
    | 5       | 1,3      | Robert Downey Jr,Scarlett Johansson |
    | 6       | 1        | Robert Downey Jr                    |
    And Producers are
    | movieId | producerId | producerName |
    | 1       | 1          | Robert1      |
    | 2       | 2          | Robert2      |
    | 3       | 3          | Robert3      |
    | 4       | 2          | Robert2      |
    | 5       | 1          | Robert1      |
    | 6       | 2          | Robert2      |