﻿Feature: ImdbProducers

@condition1
Scenario: Adding a Producer.
    Given Producer name "Tom cruze"
    And The Producer Date "03/07/2000"
    When We Add Producer To Repository
    Then Producers Table Look Like
    | Id | Name      | DOB        |
    | 1  | Robert1   | 03/07/2001 |
    | 2  | Robert2   | 03/07/2002 |
    | 3  | Robert3   | 03/07/2003 |
    | 4  | Tom cruze | 03/07/2000 |

@condition1
Scenario: Adding a null or empty Producer name.
    Given Producer name ""
    And The Producer Date "03/07/2000"
    When We Add Producer To Repository
    Then I should have an error for producers "Invalid arguments"
    And Producers Table Look Like
    | Id | Name      | DOB        |
    | 1  | Robert1   | 03/07/2001 |
    | 2  | Robert2   | 03/07/2002 |
    | 3  | Robert3   | 03/07/2003 |

@condition1
Scenario: Adding a null or empty Producer DOB.
    Given Producer name "Tom cruze"
    And The Producer Date ""
    When We Add Producer To Repository
    Then I should have an error for producers "Invalid producer DOB"
    And Producers Table Look Like
    | Id | Name      | DOB        |
    | 1  | Robert1   | 03/07/2001 |
    | 2  | Robert2   | 03/07/2002 |
    | 3  | Robert3   | 03/07/2003 |

@condition1
Scenario: Adding a Invalid Producer DOB.
    Given Producer name "Tom cruze"
    And The Producer Date "03/072000"
    When We Add Producer To Repository
    Then I should have an error for producers "Invalid producer DOB"
    And Producers Table Look Like
    | Id | Name      | DOB        |
    | 1  | Robert1   | 03/07/2001 |
    | 2  | Robert2   | 03/07/2002 |
    | 3  | Robert3   | 03/07/2003 |