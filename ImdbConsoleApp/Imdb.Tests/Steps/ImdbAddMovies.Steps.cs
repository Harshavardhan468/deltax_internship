﻿using ImdbConsoleApp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Xunit;
namespace Imdb.Tests.Steps
{
    [Binding]
    public class ImdbConsoleAppSteps
    {
        public string movieName;
        public string moviePlot;
        public string movieDate;
        public string actorsIds;
        private Exception _exception;
        public int producersId;
        public List<int> allActorsIds = new List<int>();
        public List<int> allProducersIds = new List<int>();
        ActorService actorService = new ActorService();
        ProducerService producerService = new ProducerService();
        MovieService movieService = new MovieService();
        List<Movie> allMovies = new List<Movie>();

        [BeforeScenario(new string[] { "condition1" })]
        public void BeforeSenario1()
        {
            actorService.Add("Robert Downey Jr", "04/04/1965");
            actorService.Add("Chris Evans", "13/06/1981");
            actorService.Add("Scarlett Johansson", "22/11/1984");

            producerService.Add("Robert1", "03/07/2001");
            producerService.Add("Robert2", "03/07/2002");
            producerService.Add("Robert3", "03/07/2003");

            movieService.Add("Iron Man 1", "Iron Man Created the suit", "02/05/2008", new List<int> { 1, 2 }, 1 , actorService, producerService);
            movieService.Add("Iron Man 2", "Iron Man Updated the suit", "30/04/2010", new List<int> { 2, 3 }, 2 , actorService, producerService);
            movieService.Add("Iron Man 3", "Iron Man Created the suit 2.0", "25/04/2013", new List<int> { 2, 3 }, 3, actorService, producerService);
            movieService.Add("Captain America: The First Avenger", "Captain America Born", "25/04/2013", new List<int> { 1, 2 }, 2, actorService, producerService);
            movieService.Add("captain america civil war", "Captain America Fought", "25/04/2013", new List<int> { 1, 3 },1 , actorService, producerService);
            movieService.Add("Captain America: Winter soldier", "Captain America and iron became friends", "30/04/2010", new List<int> { 1 }, 2 , actorService, producerService);
        }

        [Given(@"Movie name ""(.*)""")]
        public void GivenMovieName(string moviename)
        {
            movieName = moviename;
        }

        [Given(@"The Movie Plot ""(.*)""")]
        public void GivenTheMoviePlot(string movieplot)
        {
            moviePlot = movieplot;
        }

        [Given(@"The Movie Date ""(.*)""")]
        public void GivenTheMovieDate(string moviedate)
        {
            movieDate = moviedate;
        }

        [Given(@"The Movie Actors ""(.*)""")]
        public void GivenTheMovieActors(string movieactorsids)
        {
            actorsIds = movieactorsids;
            foreach (var actorId in actorsIds.Split(" "))
            {
                allActorsIds.Add(Convert.ToInt32(actorId));
            }
        }

        [Given(@"The Movie Producer ""(.*)""")]
        public void GivenTheMovieProducer(int movieproducerid)
        {
            producersId = movieproducerid;
        }

        [When(@"We Add Movie To Repository")]
        public void WhenWeAddMovieToRepository()
        {
            try
            {
                movieService.Add(movieName, moviePlot, movieDate, allActorsIds, producersId,actorService, producerService);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then(@"I should have an error ""(.*)""")]
        public void ThenIShouldHaveAnError(string error)
        {
            Xunit.Assert.Equal(error, _exception.Message);
        }

        [Then(@"Table Look Like")]
        public void ThenTableLookLike(Table table)
        {

            allMovies = movieService.Get();
            table.CompareToSet(allMovies);
        }

        [Then(@"Actors are")]
        public void ThenActorsAre(Table table)
        {
            var actors = allMovies.Select(x => new
            {
                movieId = x.Id,
                actorids = string.Join(",", x.Actors.Select(y => y.Id).ToList()),
                actorList = string.Join(",",x.Actors.Select(y => y.Name).ToList())
            }).ToList();
            table.CompareToSet(actors);
        }
        [Then(@"Producers are")]
        public void ThenProducersAre(Table table)
        {
            var producers = allMovies.Select(x => new
            {
                movieId = x.Id,
                producerId = x.Producer.Id,
                producerName = x.Producer.Name
            }).ToList();
            table.CompareToSet(producers);
        }
    }
}
