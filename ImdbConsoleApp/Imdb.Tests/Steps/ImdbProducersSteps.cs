﻿using ImdbConsoleApp;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.Tests.Steps
{
    [Binding]
    public class ImdbProducersSteps
    {
        public ProducerService producerService = new ProducerService();
        public Exception _exception;

        public string producerName;
        public string producerDOB;

        [BeforeScenario(new string[] { "condition1" })]
        public void BeforeSenario1()
        {
            producerService.Add("Robert1", "03/07/2001");
            producerService.Add("Robert2", "03/07/2002");
            producerService.Add("Robert3", "03/07/2003");
        }

        [Given(@"Producer name ""(.*)""")]
        public void GivenProducerName(string producername)
        {
            producerName = producername;
        }
        
        [Given(@"The Producer Date ""(.*)""")]
        public void GivenTheProducerDate(string producerdob)
        {
            producerDOB = producerdob;
        }
        
        [When(@"We Add Producer To Repository")]
        public void WhenWeAddProducerToRepository()
        {
            try
            {
                producerService.Add(producerName, producerDOB);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }
        
        [Then(@"Producers Table Look Like")]
        public void ThenProducersTableLookLike(Table table)
        {
            var allProducers = producerService.Get();
            table.CompareToSet(allProducers);
        }
        
        [Then(@"I should have an error for producers ""(.*)""")]
        public void ThenIShouldHaveAnErrorForProducers(string error)
        {
            Xunit.Assert.Equal(error, _exception.Message);
        }
    }
}
