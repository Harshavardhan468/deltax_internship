﻿using ImdbConsoleApp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Xunit;

namespace Imdb.Tests.Steps
{
    [Binding]
    public class ImdbAActorsSteps
    {
        ActorService actorService = new ActorService();
        Exception _exception;

        string actorName;
        string actorDOB;

        [BeforeScenario(new string[] { "condition1" })]
        public void BeforeSenario1()
        {
            actorService.Add("Robert Downey Jr", "04/04/1965");
            actorService.Add("Chris Evans", "13/06/1981");
            actorService.Add("Scarlett Johansson", "22/11/1984");
        }

        [Given(@"Actor name ""(.*)""")]
        public void GivenActorName(string actorname)
        {
            actorName = actorname;
        }
        
        [Given(@"The Actor Date ""(.*)""")]
        public void GivenTheActorDate(string actordob)
        {
            actorDOB = actordob;
        }
        
        [When(@"We Add Actor To Repository")]
        public void WhenWeAddActorToRepository()
        {
            try
            {
                actorService.Add(actorName, actorDOB);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then(@"I should have an error for actors""(.*)""")]
        public void ThenIShouldHaveAnErrorForActors(string error)
        {
            Xunit.Assert.Equal(error, _exception.Message);
        }

        [Then(@"Actors Table Look Like")]
        public void ThenActorsTableLookLike(Table table)
        {
            var allActors = actorService.Get();
            table.CompareToSet(allActors);
        }
    }
}
