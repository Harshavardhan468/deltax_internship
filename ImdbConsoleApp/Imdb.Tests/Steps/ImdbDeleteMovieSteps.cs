﻿using ImdbConsoleApp;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.Tests.Steps
{
    [Binding]
    public class ImdbDeleteMovieSteps
    {
        public int movieId;
        private Exception _exception;
        public string producersIds;
        public List<int> allActorsIds = new List<int>();
        public List<int> allProducersIds = new List<int>();
        ActorService actorService = new ActorService();
        ProducerService producerService = new ProducerService();
        MovieService movieService = new MovieService();
        List<Movie> allMovies = new List<Movie>();

        [BeforeScenario(new string[] { "condition1" })]
        public void BeforeSenario1()
        {
            actorService.Add("Robert Downey Jr", "04/04/1965");
            actorService.Add("Chris Evans", "13/06/1981");
            actorService.Add("Scarlett Johansson", "22/11/1984");

            producerService.Add("Robert1", "03/07/2001");
            producerService.Add("Robert2", "03/07/2002");
            producerService.Add("Robert3", "03/07/2003");

            movieService.Add("Iron Man 1", "Iron Man Created the suit", "02/05/2008", new List<int> { 1, 2 }, 1 , actorService, producerService);
            movieService.Add("Iron Man 2", "Iron Man Updated the suit", "30/04/2010", new List<int> { 2, 3 }, 2 , actorService, producerService);
            movieService.Add("Iron Man 3", "Iron Man Created the suit 2.0", "25/04/2013", new List<int> { 2, 3 }, 3 , actorService, producerService);
            movieService.Add("Captain America: The First Avenger", "Captain America Born", "25/04/2013", new List<int> { 1, 2 }, 2 , actorService, producerService);
            movieService.Add("captain america civil war", "Captain America Fought", "25/04/2013", new List<int> { 1, 3 }, 1 , actorService, producerService);
            movieService.Add("Captain America: Winter soldier", "Captain America and iron became friends", "30/04/2010", new List<int> { 1 }, 2 , actorService, producerService);
        }

        [Given(@"Movie Id ""(.*)""")]
        public void GivenMovieId(int movieid)
        {
            movieId = movieid;
        }
        [When(@"We Delete Movie From Repository")]
        public void WhenWeDeleteMovieFromRepository()
        {
            try
            {
                movieService.Delete(movieId);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }
        [Then(@"Movie Table Look Like")]
        public void ThenMovieTableLookLike(Table table)
        {
            allMovies = movieService.Get();
            table.CompareToSet(allMovies);
        }

        [Then(@"Actors are After Deleting")]
        public void ThenActorsAreAfterDeleting(Table table)
        {
            var actors = allMovies.Select(x => new
            {
                movieId = x.Id,
                actorList = string.Join(",", x.Actors.Select(y => y.Name).ToList())
            }).ToList();
            table.CompareToSet(actors);
        }
        [Then(@"Producers are After Deleting")]
        public void ThenProducersAreAfterDeleting(Table table)
        {
            var producers = allMovies.Select(x => new
            {
                movieId = x.Id,
                producerId = x.Producer.Id,
                producerName = x.Producer.Name
            }).ToList();
            table.CompareToSet(producers);
        }

        [Then(@"I should have an error for Movies ""(.*)""")]
        public void ThenIShouldHaveAnErrorForMovies(string error)
        {
            Xunit.Assert.Equal(error, _exception.Message);
        }
    }
}