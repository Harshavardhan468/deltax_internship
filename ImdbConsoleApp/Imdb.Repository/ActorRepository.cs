﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImdbConsoleApp
{
    public class ActorRepository
    {
        private List<Actor> listOfActors = new List<Actor>();
        public void Add(Actor actor)
        {
            listOfActors.Add(actor);
        }
        public List<Actor> GetAll()
        {
            return listOfActors;
        }

        public List<int> GetIds()
        {
            var actorIds = listOfActors.Select(x => x.Id).ToList();
            return actorIds;
        }
    }
}
