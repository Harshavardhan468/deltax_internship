﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImdbConsoleApp
{
    public class ProducerRepository
    {
        private List<Producer> listOfProducers = new List<Producer>();
        public void Add(Producer producer)
        {
            listOfProducers.Add(producer);
        }
        public List<Producer> GetAll()
        {
            return listOfProducers;
        }
        public List<int> GetIds()
        {
            var producerIds = listOfProducers.Select(x => x.Id).ToList();
            return producerIds;
        }
    }
}
