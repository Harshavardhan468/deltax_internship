﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImdbConsoleApp
{
    public class MovieRepository
    {
        public List<Movie> listOfMovies = new List<Movie>();
        public void Add(Movie movie)
        {
            listOfMovies.Add(movie);
        }

        public List<Movie> Get()
        {
            return listOfMovies;
        }

        public void RemoveById(int movieId)
        {
            listOfMovies.RemoveAll(movie => movie.Id == movieId);
        }
        public List<int> GetIds()
        {
            var movieIds = listOfMovies.Select(x=>x.Id).ToList();
            return movieIds;
        }
    }
}
