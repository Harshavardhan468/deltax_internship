﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ImdbConsoleApp
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name{get; set;}
        public string Date { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; }
        public Producer Producer { get; set; }
        public Movie(int movieId, string movieName, string moviePlot, string movieDate, List<Actor> actors, Producer producer)
        {
            Id = movieId;
            Name = movieName;
            Plot = moviePlot;
            Date  = movieDate;
            Actors = actors;
            Producer = producer;
        }
    }
}
