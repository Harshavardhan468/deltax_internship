﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbConsoleApp
{
    public class Person
    {
        public string Name { get; set; }
        public string DOB { get; set; }
        public int Id { get; set; }
    }
}
