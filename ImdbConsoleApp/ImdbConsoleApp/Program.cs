﻿using System;
using System.Collections.Generic;

namespace ImdbConsoleApp
{
    public class Program
    {        
        public static void Main(string[] args)
        {
            
            int option;
            bool showMenu = true;
            string feedback;
            Console.WriteLine("Welcome To Imdb Console App!!");
            Validators validators = new Validators();
            ActorService actorService = new ActorService();
            ProducerService producerService = new ProducerService();
            MovieService movieService = new MovieService();

            actorService.Add("Robert Downey Jr", "04/04/1965");
            actorService.Add("Chris Evans", "13/06/1981");
            actorService.Add("Scarlett Johansson", "22/11/1984");

            producerService.Add("Robert1", "03/07/2001");
            producerService.Add("Robert2", "03/07/2002");
            producerService.Add("Robert3", "03/07/2003");

            movieService.Add("Iron Man 1", "Iron Man Created the suit", "02/05/2008", new List<int>{ 1, 2 }, 1 , actorService, producerService);
            movieService.Add("Iron Man 2", "Iron Man Updated the suit", "30/04/2010", new List<int> { 3, 2 }, 2 , actorService, producerService);
            movieService.Add("Iron Man 3", "Iron Man Created the suit 2.0", "25/04/2013", new List<int> { 2, 3 }, 3 , actorService, producerService);
            movieService.Add("Captain America: The First Avenger", "Captain America Born", "25/04/2013", new List<int> { 3, 2 }, 2  ,actorService, producerService);
            movieService.Add("captain america civil war", "Captain America Fought", "25/04/2013", new List<int> { 3, 1 }, 1 ,actorService, producerService);
            movieService.Add("Captain America: Winter soldier", "Captain America and iron became friends", "30/04/2010", new List<int>{ 1}, 2 ,actorService, producerService);

            while (showMenu)
            {
                Console.WriteLine("1: List of Movies");
                Console.WriteLine("2: Add Movie");
                Console.WriteLine("3: Add Actor");
                Console.WriteLine("4: Add Producer");
                Console.WriteLine("5: Delete Movie");
                Console.WriteLine("6: Exit");

                option = validators.OptionValidation();

                switch (option)
                {
                    case 1:
                        var allMovies = movieService.Get();
                        var movieCount = movieService.GetCount();
                        if (movieCount == 0)
                        {
                            Console.WriteLine("No movies Available");
                        }
                        else
                        {
                            foreach (var movieDetails in allMovies)
                            {
                                Console.WriteLine("Movie Id: " + movieDetails.Id + "\n" + "Movie Name: " + movieDetails.Name + "\n"
                                    + "Movie Plot: " + movieDetails.Plot + "\n" + "Movie Date: " + movieDetails.Date );

                                Console.WriteLine("Movie Actors: ");
                                foreach (var actor in movieDetails.Actors)
                                {
                                    Console.WriteLine(actor.Name);
                                }

                                Console.WriteLine("Movie Producer: ");
                                Console.WriteLine(movieDetails.Producer.Name);
                            }
                        }
                        break;

                    case 2:
                        bool isActorsEmpty = actorService.GetCount()==0;
                        bool isProducersEmpty = producerService.GetCount()==0;
                        if (isActorsEmpty || isProducersEmpty)
                        {
                            if (isActorsEmpty)
                            {
                                Console.WriteLine("No actors added please add actors.");
                            }
                            if (isProducersEmpty)
                            {
                                Console.WriteLine("No producers added please add producers");
                            }
                        }
                        else
                        {
                            string movieName = validators.GetInput("Movie Name");
                            string moviePlot = validators.GetInput("Movie Plot");
                            string movieDate = validators.DateInputAndValidation("Movie Date ");

                            var allActors = actorService.Get();
                            var allActorsIds = actorService.GetIds();
                            foreach (var actor in allActors)
                            {
                                Console.WriteLine(actor.Id + ")" + actor.Name);
                            }
                            var allMovieActors = validators.GetInputAndCheckActors(allActorsIds, "Movie Actors Ids with spaces");


                            var allProducers = producerService.Get();
                            var allProducersIds = producerService.GetIds();

                            foreach (var producer in allProducers)
                            {
                                Console.WriteLine(producer.Id + ")" + producer.Name);
                            }

                            var allMovieProducers = validators.GetInputAndCheckProducers(allProducersIds, "Movie Producer Id");

                            feedback = movieService.Add(movieName, moviePlot, movieDate, allMovieActors, allMovieProducers, actorService, producerService);
                            Console.WriteLine(feedback);
                        }
                        break;

                    case 3:
                        string actorName = validators.GetInput("Actor Name");

                        string actorDOB = validators.DateInputAndValidation("Actor DOB ");
                        try
                        {
                            feedback = actorService.Add(actorName, actorDOB);
                            Console.WriteLine(feedback);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;

                    case 4:

                        string producerName = validators.GetInput("Producer Name");
                        string producerDOB = validators.DateInputAndValidation("Producer DOB ");

                        try
                        {
                            feedback = producerService.Add(producerName, producerDOB);
                            Console.WriteLine(feedback);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        break;

                    case 5:
                        var movieIds = movieService.GetIds();
                        int movieId = validators.GetInputAndCheckMovie(movieIds, "Enter Movie Id to be removed: ");
                        feedback = movieService.Delete(movieId);
                        Console.WriteLine(feedback);
                        break;

                    case 6:
                        showMenu = false;
                        break;

                }
            }
        }
    }
}