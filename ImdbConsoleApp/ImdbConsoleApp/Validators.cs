﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ImdbConsoleApp
{
    class Validators
    {
        public string GetInput(string message)
        {
            string value;
            Console.Write("Enter " + message + ": ");
            value = Console.ReadLine();
            if (!string.IsNullOrEmpty(value))
                return value;
            else
            {
                Console.WriteLine("Invalid " + message + " please ReEnter");
                return GetInput(message);
            }

        }

        public string DateInputAndValidation(string message)
        {
            string date;
            Console.Write("Enter " + message + "in (DD/MM/YYYY) Format: ");
            date = Console.ReadLine();
            DateTime dt;
            var isDateValid = DateTime.TryParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            if (!isDateValid || Convert.ToDateTime(date) < Convert.ToDateTime("01/01/1970") || Convert.ToDateTime(date) > DateTime.Now)
            {
                Console.WriteLine("Invalid " + message + " please ReEnter");
                return DateInputAndValidation(message);
            }
            else
                return date;
        }

        public int OptionValidation()
        {
            int option;
            option = Convert.ToInt32(Console.ReadLine());
            if (option >= 1 || option <= 6)
                return option;
            else
            {
                Console.WriteLine("Invalid option please ReEnter");
                return OptionValidation();
            }
        }

        public int GetInputAndCheckMovie(List<int> movieIds, string message)
        {
            Console.WriteLine(message);
            int movieId = Convert.ToInt32(Console.ReadLine());
            if (movieIds.Contains(movieId))
                return movieId;
            else
            {
                Console.WriteLine("Movie Id Doesn't Exists - Please ReEnter");
                return GetInputAndCheckMovie(movieIds, message);
            }
        }

        public List<int> GetInputAndCheckActors(List<int> allactorsIds, string message)
        {
            Console.WriteLine("Enter " + message + ": ");
            List<int> ActorsIds = new List<int>();
            string movieActorsIds = Console.ReadLine();
            int flag = 0;
            foreach (var actorId in movieActorsIds.Split(" "))
            {
                if (!allactorsIds.Contains(Convert.ToInt32(actorId)))
                {
                    flag = 1;
                    break;
                }
                ActorsIds.Add(Convert.ToInt32(actorId));
            }
            if (flag == 0)
                return ActorsIds;
            else
            {
                Console.WriteLine("Invalid actor ids - Please ReEnter");
                return GetInputAndCheckActors(allactorsIds, message);
            }
        }

        public int GetInputAndCheckProducers(List<int> allProducersId, string message)
        {
            Console.WriteLine("Enter "+message+": ");
            int movieProducerId;
            try
            {
                movieProducerId = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Invalid producer id - Please ReEnter");
                return GetInputAndCheckProducers(allProducersId, message);
            }
            if (allProducersId.Contains(movieProducerId))
            {
                return movieProducerId;
            }
            else
            {
                Console.WriteLine("Invalid producer id - Please ReEnter");
                return GetInputAndCheckProducers(allProducersId, message);
            }
        }
    }
}