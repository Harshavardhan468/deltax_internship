﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbConsoleApp.IService
{
    interface IActorService
    {
        public List<int> GetIds();
        public int GetCount();
        public string Add(string actorName, string actorDOB);
        public List<Actor> Get();
        public bool CheckDate(String dateOfbirth);

    }
}
