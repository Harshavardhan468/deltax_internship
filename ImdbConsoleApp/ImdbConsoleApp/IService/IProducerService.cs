﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbConsoleApp.IService
{
    interface IProducerService
    {
        public List<int> GetIds();
        public int GetCount();
        public string Add(string producerName, string producerDOB);
        public bool CheckDate(String dateOfbirth);
        public List<Producer> Get();


    }
}
