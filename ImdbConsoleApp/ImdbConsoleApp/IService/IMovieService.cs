﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImdbConsoleApp.IService
{
    interface IMovieService
    {
        public List<int> GetIds();
        public string Delete(int movieId);
        public int GetCount();
        public string Add(string movieName, string moviePlot, string movieDate, List<int> movieActorsIds, int movieProducersId, ActorService actorService, ProducerService producerService);
        public List<Movie> Get();
    }
}
