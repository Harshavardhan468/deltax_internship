﻿using ImdbConsoleApp.IService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ImdbConsoleApp
{
    public class MovieService : IMovieService
    {
        public MovieRepository movieRepository = new MovieRepository();
        public int movieId = 1;
        public bool CheckDate(String dateOfbirth)
        {
            var actorDOB = dateOfbirth;
            DateTime dt;
            var isDateValid = DateTime.TryParseExact(actorDOB,"dd/MM/yyyy",CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            return isDateValid;
        }
        public List<Movie> Get()
        {
            return movieRepository.Get();
        }
        
        public string Add(string movieName, string moviePlot, string movieDate, List<int> movieActorsIds, int movieProducersId, ActorService actorService, ProducerService producerService)
        {
            if (!CheckDate(movieDate))
            {
                throw new ArgumentException("Invalid movie date");
            }
            else if (string.IsNullOrEmpty(movieName) || string.IsNullOrEmpty(moviePlot) || string.IsNullOrEmpty(movieDate) )
            {
                throw new ArgumentException("Invalid arguments");
            }
            else
            {
                var movieActorsObjects = actorService.actorRepository.GetAll().Where(x=>movieActorsIds.Contains(x.Id)).ToList();

                var movieProducersObject = producerService.producerRepository.GetAll().Single(x => movieProducersId == x.Id);

                var movie = new Movie(movieId++, movieName, moviePlot, movieDate, movieActorsObjects, movieProducersObject);
                movieRepository.Add(movie);
            }
            return "Movie Added Sucessfully!";
        }

        public int GetCount()
        {
            return Get().Count;
        }

        public string Delete(int movieId)
        {
            List<int> movieIds = GetIds();
            if (movieIds.Contains(movieId))
            {
                movieRepository.RemoveById(movieId);
            }
            else
            {
                throw new ArgumentException("Movie Id Doesn't exists");
            }
            return "Movie Deleted Sucessfully";
        }

        public List<int> GetIds()
        {
            return movieRepository.GetIds();
        }
    }
}