﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ImdbConsoleApp
{
    public class ProducerService
    {
        public ProducerRepository producerRepository = new ProducerRepository();
        public int producerId = 1;
        public List<Producer> Get()
        {
            return producerRepository.GetAll();
        }

        public bool CheckDate(String dateOfbirth)
        {
            var actorDOB = dateOfbirth;
            DateTime dt;
            var isDateValid = DateTime.TryParseExact(actorDOB, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            return isDateValid;
        }

        public string Add(string producerName, string producerDOB)
        {
            if (!CheckDate(producerDOB))
            {
                throw new ArgumentException("Invalid producer DOB");
            }
            else if (string.IsNullOrEmpty(producerName) || string.IsNullOrEmpty(producerDOB))
            {
                throw new ArgumentException("Invalid arguments");
            }
            else
            {
                var producer = new Producer();
                producer.Id = producerId++;
                producer.Name = producerName;
                producer.DOB = producerDOB;
                producerRepository.Add(producer);
            }
            return "Added Producer Sucessfully";
        }

        public int GetCount()
        {
            return Get().Count;
        }
        public List<int> GetIds()
        {
            return producerRepository.GetIds();
        }
    }
}
