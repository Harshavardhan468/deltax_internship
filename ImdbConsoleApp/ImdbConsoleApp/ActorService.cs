﻿using ImdbConsoleApp.IService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ImdbConsoleApp
{
    public class ActorService : IActorService
    {
        public ActorRepository actorRepository = new ActorRepository();
        public int actorId = 1;
        public bool CheckDate(String dateOfbirth)
        {
            var actorDOB = dateOfbirth;
            DateTime dt;
            var isDateValid = DateTime.TryParseExact(actorDOB, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            return isDateValid;
        }

        public List<Actor> Get()
        {

            return actorRepository.GetAll();
        }

        public string Add( string actorName, string actorDOB )
        {
            if (!CheckDate(actorDOB))
            {
                throw new ArgumentException("Invalid actor DOB");
            }
            else if ( string.IsNullOrEmpty(actorName) || string.IsNullOrEmpty(actorDOB) )
            {
                throw new ArgumentException("Invalid arguments");
            }

            var actor = new Actor();
            actor.Id = actorId++;
            actor.Name = actorName;
            actor.DOB = actorDOB;
            actorRepository.Add(actor);
            return "Added Actor Sucessfully";
        }

        public int GetCount()
        {
            return Get().Count;
        }

        public List<int> GetIds()
        {
            return actorRepository.GetIds();
        }
    }
}